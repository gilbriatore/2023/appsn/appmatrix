package br.edu.up.app.data.model

data class Pessoa(
    val nome: String,
    val cpf: String,
    val foto: Int
)