package br.edu.up.app.ui.gallery

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import br.edu.up.app.R
import br.edu.up.app.databinding.FragmentGalleryBinding

class GalleryFragment : Fragment() {

    private var _binding: FragmentGalleryBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val galleryViewModel =
            ViewModelProvider(this).get(GalleryViewModel::class.java)

        _binding = FragmentGalleryBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val estados = mapOf(
            "AC" to "Acre",
            "AL" to "Alagoas",
            "AM" to "Amazonas",
            "AP" to "Amapá",
            "BA" to "Bahia",
            "CE" to "Ceará",
            "DF" to "Distrito Federal",
            "ES" to "Espírito Santo",
            "GO" to "Goiás",
            "MA" to "Maranhão",
            "MG" to "Minas Gerais",
            "MS" to "Mato Grosso do Sul",
            "MT" to "Mato Grosso",
            "PA" to "Pará",
            "PB" to "Paraíba",
            "PE" to "Pernambuco",
            "PI" to "Piauí",
            "PR" to "Paraná",
            "RJ" to "Rio de Janeiro",
            "RN" to "Rio Grande do Norte",
            "RO" to "Rondônia",
            "RR" to "Roraima",
            "RS" to "Rio Grande do Sul",
            "SC" to "Santa Catarina",
            "SE" to "Sergipe",
            "SP" to "São Paulo",
            "TO" to "Tocantins"
        )

        val spinner: Spinner = binding.spinner
        val ctx = this.requireContext()
        val adapter = ArrayAdapter.createFromResource(
            ctx, R.array.siglas, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item )
        spinner.adapter = adapter

        val siglas = resources.getStringArray(R.array.siglas)
        spinner.onItemSelectedListener = object : OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
                val estado = estados.get( siglas[pos])
                Toast.makeText(ctx, estado, Toast.LENGTH_LONG).show()
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}