package br.edu.up.app.ui

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import br.edu.up.app.databinding.ItemPessoaBinding
import br.edu.up.app.ui.placeholder.PlaceholderContent.PlaceholderItem


/**
 * [RecyclerView.Adapter] that can display a [PlaceholderItem].
 * TODO: Replace the implementation with code for your data type.
 */
class PessoasAdapter(
    private val values: List<PlaceholderItem>
) : RecyclerView.Adapter<PessoasAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ItemPessoaBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.txtNome.text = item.id
        holder.txtCpf.text = item.content
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: ItemPessoaBinding) : RecyclerView.ViewHolder(binding.root) {
        val txtNome: TextView = binding.txtNome
        val txtCpf: TextView = binding.txtCpf
    }

}