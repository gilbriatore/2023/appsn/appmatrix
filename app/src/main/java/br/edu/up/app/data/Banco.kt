package br.edu.up.app.data

import br.edu.up.app.R
import br.edu.up.app.data.model.Pessoa
import kotlin.random.Random

class Banco {

    fun getPessoas(): MutableList<Pessoa>{
        val avatares = listOf(
            R.drawable.avatar01,
            R.drawable.avatar02,
            R.drawable.avatar03,
            R.drawable.avatar04,
            R.drawable.avatar04
        )

        val lista = mutableListOf<Pessoa>()
        for(i in 1..20){
            val foto = Random.nextInt(4)
            lista.add(Pessoa("Nome $i", "CPF $i", foto))
        }
        return lista
    }
}